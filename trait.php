<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

trait Security {
    private function showName() {
	echo "I am security class";
    }
    
    public function defineMilage() {
	echo "20M/L";
    }
    
    function showPerformance() {
	echo "i am super performer from security";
    }
}

trait Performance {
    
    public $name;
    
    function showPerformance() {
	echo "i am super performer";
    }
    
    
}




Class BMWX7 {
    
    use Security, Performance {
	Security::showPerformance as test;
	Performance::showPerformance insteadof Security;
	showName as public;
    }
    
    

}

$a = new BMWX7;

echo $a->showPerformance();
echo $a->showName();