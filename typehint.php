<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

interface Transport {
    public function showMe();
}

class Car implements Transport{
    protected $brand;
    public $type;
    public $year;
    public $model;
    public $cc;
    
    public function __construct($brand, $type, $year, $model, $cc) {
	$this->brand = $brand;
	$this->type = $type;
	$this->year = $year;
	$this->model = $model;
	$this->cc = $cc;
    }
    
    public function showMe() {
	echo "From car class ".$this->brand;
    }
    
}

class BMW {
    
    
    protected $cost;
    
    
    function getModel(Transport $car) {
	return $car->model;
    }
    
    
}

$car = new Car("bmw","sedan","2016", "x3","2400");

$bmw = new BMW;

echo $bmw->getModel($car);
