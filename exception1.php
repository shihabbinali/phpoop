<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class TMCustomException_NonZeroNumber extends Exception {
    
};

class TMCustomException_NonNegativeNumber extends Exception {
    
};

try {
    $i = $_GET['i'];
    $j = $_GET['j'];

    try {
	if ($j == 0) {
	    throw new TMCustomException_NonZeroNumber("Can not divide by 0");
	} else if ($j < 0) {
	    throw new TMCustomException_NonNegativeNumber("Can not be negative");
	} else
	    echo $i / $j;
    }  catch (Exception $e) {
	echo "I have found an exception";

	//print_r($e);

	echo "<Br />" . $e->getMessage();
    }


    echo "testing done";
} catch (TMCustomException_NonZeroNumber $e) {
    echo "I have found an exception through TM custom exception handler - outside";

    //print_r($e);

    echo "<Br />" . $e->getMessage();
} catch (TMCustomException_NonNegativeNumber $e) {
    echo "I have found another exception through TM custom exception handler - outside";

    //print_r($e);

    echo "<Br />" . $e->getMessage();
} catch (Exception $e) {
    echo "I have found an exception - outside";

    //print_r($e);

    echo "<Br />" . $e->getMessage();
} finally {
    
}

echo "everything is done finally";