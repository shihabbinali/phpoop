<?php
class Cricketer {
    
    public $name;
    public $position;
    public $age;
    
    public function __construct($name, $position, $age) {
        $this->name = $name;
        $this->position = $position;
        $this->age = $age;
    }
    
    public static function showName() {
        echo static::getName();
    }
    
    public static function getName() {
        return "Mizan";
    }
    
}

class NationalCricketer extends Cricketer {
    
    public $division;
    
    public function __construct($name, $position, $age, $division) {
        parent::__construct($name, $position, $age);
        $this->division = $division;
    }
       

    public static function getName() {
        return "National Cricketer";
    }
    
}


$mosaddek = new Cricketer("Mosaddek Saikat", "All Rounder", 20);
$mash = new Cricketer( "Masrafee", "Bowler", 32);

$mash::showName()."<br />";

$nafis = new NationalCricketer("Shariar Nafis", "Opening Batsman", 30, "Barisal");

$nafis::showName();