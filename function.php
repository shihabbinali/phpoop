<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

echo '<pre>';

$arr = ["abc123","def124","ghi100"];

$len = 3;
$name = "";

usort($arr, function($val1, $val2) use($len,$name) {
    echo $val1. " > ".$val2."<br />";
    $num1 = substr($val1,-$len);
    $num2 = substr($val2,-$len);
    
    return $num1 > $num2;
});

print_r($arr);


$arr = range(1,20);
$arr[10] = [range(31,40),range(20,35)];
$arr[20][10][10][30][50][100] = 6;
print_r($arr);


function my_array_sum($arr) {
    $sum = 0;
    $len = count($arr);
    foreach($arr as $val) {
	if(is_array($val))
	    $sum = my_array_sum($val) + $sum;
	else
	    $sum = $val + $sum;
    }
    
    return $sum;
}



echo my_array_sum($arr);

function factorial($num) {
    
    if($num == 1) return 1; // boundary condition
    
    return $num * factorial($num-1);
    
}

echo "\n".factorial(10);