<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

final class Car {
    public $brand;
    public $type;
    public $year;
    public $model = 10;
    public $cc;
    
    public function __construct($brand, $type, $year, $model, $cc) {
	$this->brand = $brand;
	$this->type = $type;
	$this->year = $year;
	$this->model = $model;
	$this->cc = $cc;
    }
    
    public function showMe() {
	echo "From car class ".$this->brand;
    }
    
}

class BMW extends Car{
    
    
    protected $cost;
    public $color;
    
    public function __construct($brand, $type, $year, $model, $cc, $cost, $color) {
	parent::__construct($brand, $type, $year, $model, $cc);
	$this->cost = $cost;
	$this->color = $color;
    }
    
    
    function getModel() {
	return $this->model;
    }
    
    public function setModel($model = "") {
	$this->model = $model;
    }
    
    public function showMe() {
	echo "From BMW class ".$this->brand;
	parent::showMe();
    }
    
    
    function __call($name, $arguments) {
	if($name == "showMilage") 
	    echo "i do not have $name";
    }
    
    
    function __clone() {
	//echo "i am cloning";
	//$this->color = new color($this->color->red, $this->color->green, $this->color->blue);
	
	foreach($this as $property => $value) {
	    if(is_object($value) || is_array($value)) {
		$this->{$property} = unserialize(serialize($value));
	    }
	}
    }
    
}

class color {
    public $red;
    public $green;
    public $blue;
    
    public function __construct($red, $green, $blue) {
	$this->red = $red;
	$this->green = $green;
	$this->blue = $blue;
    }
    
}

$navyblue = new Color(50,50,240);

$bmw1 = new BMW("bmw","sedan","2016", "x3","2400","80K", $navyblue);



print_r($bmw1);

$bmw2 = clone $bmw1;

$bmw2->model = "X7";

$bmw2->color->red = 255;

print_r($bmw1);
print_r($bmw2);